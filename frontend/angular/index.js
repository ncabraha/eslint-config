module.exports = {
    extends: 'jhtna/lib/defaults',

    globals: {
        'angular': true
    },

    env: {
      browser: true
    }
};