// Any backend/frontend/framework agnostic configurations

module.exports = {
    "extends": "eslint:recommended",

    globals: {
        Promise: true
    },

    // some options:
    // operator-linebreak
    // require-jsdoc

    rules: {
        indent: 2,
        'object-curly-spacing': [2, 'always'],
        'space-before-blocks': [2, 'always'],
        'space-after-keywords': [2, 'always'],
        'no-spaced-func': 2,
        'space-before-function-paren': [2, 'never'],
        'comma-dangle': [2, 'never'],
        'block-scoped-var': 2,
        curly: 2,
        'default-case': 2,
        'dot-location': [2, 'property'],
        'dot-notation': [2, { allowPattern: '^[a-z]+(_[a-z]+)+$' }], // allow: obj['snake_case']
        eqeqeq: [2, 'allow-null'],
        'guard-for-in': 2,
        'no-alert': 2,
        'no-caller': 2,
        'no-eval': 2,
        'no-extend-native': 2,
        'no-extra-bind': 2,
        'no-floating-decimal': 2,
        'no-implied-eval': 2,
        'no-iterator': 2,
        'no-labels': 2,
        'no-lone-blocks': 2,
        'no-loop-func': 2,
        'no-multi-spaces': 2,
        'no-multi-str': 2,
        'no-native-reassign': 2,
        'no-new-func': 2,
        'no-new-wrappers': 2,
        'no-new': 2,
        'no-param-reassign': 2,
        'no-return-assign': 2,
        'no-script-url': 2,
        'no-self-compare': 2,
        'no-throw-literal': 2,
        'no-unused-expressions': 2,
        'no-useless-call': 2,
        'no-useless-concat': 2,
        'no-void': 2,
        'no-with': 2,
        radix: 2,
        'wrap-iife': [2, 'outside'],
        yoda: 2,
        'brace-style': [2, '1tbs', { allowSingleLine: true }],
        'comma-style': 2,
        'consistent-this': [2, 'that'],
        'linebreak-style': 2,
        'new-parens': 2,
        'lines-around-comment': 2,
        'no-array-constructor': 2,
        'no-nested-ternary': 2,
        'no-trailing-spaces': 2,
        // 'no-underscore-dangle': 2,
        'no-unneeded-ternary': 2,
        'operator-assignment': [2, 'always'],
        'quote-props': [2, 'as-needed'],
        quotes: [2, 'single', 'avoid-escape'],
        'semi-spacing': 2,
        semi: [2, 'always'],
        'space-return-throw-case': 2,
        'space-unary-ops': 2,
        'spaced-comment': 2
    }

};
